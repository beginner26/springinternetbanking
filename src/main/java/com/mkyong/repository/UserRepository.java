package com.mkyong.repository;

import com.mkyong.model.Book;
import com.mkyong.model.Transactions;
import com.mkyong.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    Optional<User> findById(Long id);

    List<Transactions> findAll(Long id);

    int update(User user);
}
