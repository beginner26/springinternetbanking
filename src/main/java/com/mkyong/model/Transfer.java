package com.mkyong.model;

public class Transfer {

    long idPengirim;
    long idPenerima;
    int saldoPengirim;
    int saldoPenerima;

    String tanggal;

    public Transfer(long idPengirim, long idPenerima, int saldoPengirim, int saldoPenerima, String tanggal) {
        this.idPengirim = idPengirim;
        this.idPenerima = idPenerima;
        this.saldoPengirim = saldoPengirim;
        this.saldoPenerima = saldoPenerima;
        this.tanggal = tanggal;
    }

    public long getIdPengirim() {
        return idPengirim;
    }

    public void setIdPengirim(long idPengirim) {
        this.idPengirim = idPengirim;
    }

    public long getIdPenerima() {
        return idPenerima;
    }

    public void setIdPenerima(long idPenerima) {
        this.idPenerima = idPenerima;
    }

    public int getSaldoPengirim() {
        return saldoPengirim;
    }

    public void setSaldoPengirim(int saldoPengirim) {
        this.saldoPengirim = saldoPengirim;
    }

    public int getSaldoPenerima() {
        return saldoPenerima;
    }

    public void setSaldoPenerima(int saldoPenerima) {
        this.saldoPenerima = saldoPenerima;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
