package com.mkyong.model;

public class Transactions {
    private String transDate;
    private String transDesc;
    private int trans_amount;
    private int customerId;

    public Transactions() {

    }

    public Transactions(String transDate, String transDesc, int trans_amount, int customerId) {
        this.transDate = transDate;
        this.transDesc = transDesc;
        this.trans_amount = trans_amount;
        this.customerId = customerId;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTransDesc() {
        return transDesc;
    }

    public void setTransDesc(String transDesc) {
        this.transDesc = transDesc;
    }

    public int getTrans_amount() {
        return trans_amount;
    }

    public void setTrans_amount(int trans_amount) {
        this.trans_amount = trans_amount;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
