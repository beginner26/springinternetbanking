package com.mkyong.model;

import java.io.Serializable;

/**
 * JavaBean class used in jsp action tags.
 * @author Ramesh Fadatare
 */
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private String nama;
	private String nomorHandphone;
	private String username;
	private String password;
	private int pin;
	private int saldo;

	public User() {
	}

	public User(Long id, String nama, String nomorHandphone, String username, String password, int pin, int saldo) {
		this.id = id;
		this.nama = nama;
		this.nomorHandphone = nomorHandphone;
		this.username = username;
		this.password = password;
		this.pin = pin;
		this.saldo = saldo;

	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNomorHandphone() {
		return nomorHandphone;
	}

	public void setNomorHandphone(String nomorHandphone) {
		this.nomorHandphone = nomorHandphone;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
